package hsplug

type Plug struct {
	IPAddress string
}

type SysInfo struct {
	ErrCode    int    `json:"err_code"`
	SwVer      string `json:"sw_ver"`
	HwVer      string `json:"hw_ver"`
	MicType    string `json:"mic_type"`
	Model      string `json:"model"`
	Mac        string `json:"mac"`
	DeviceID   string `json:"deviceId"`
	HwID       string `json:"hwId"`
	FwID       string `json:"fwId"`
	OemID      string `json:"oemId"`
	Alias      string `json:"alias"`
	DevName    string `json:"dev_name"`
	IconHash   string `json:"icon_hash"`
	RelayState int    `json:"relay_state"`
	OnTime     int    `json:"on_time"`
	ActiveMode string `json:"active_mode"`
	Feature    string `json:"feature"`
	Updating   int    `json:"updating"`
	Rssi       int    `json:"rssi"`
	LedOff     int    `json:"led_off"`
	Latitude   int    `json:"latitude"`
	Longitude  int    `json:"longitude"`
}

type Info struct {
	System struct {
		SysInfo SysInfo `json:"get_sysinfo"`
	} `json:"system"`
}
