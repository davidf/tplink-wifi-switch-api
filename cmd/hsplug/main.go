package main

import (
	"fmt"
	"os"
	"flag"
	"path/filepath"

	"tplink-wifi-switch-api/hsplug"
)

func usage() {
	fmt.Fprintf(os.Stderr, "HSPlug command line interface\n\n")
	fmt.Fprintf(os.Stderr, "Usage: %s [options] <ip address> <on or off>\n\n",
	filepath.Base(os.Args[0]))
	flag.PrintDefaults()
	fmt.Fprintf(os.Stderr, "\n")
}

func main() {
	//var help bool
	//flag.BoolVar(&help, "help", false, "Show help message")
	flag.Usage = usage
	flag.Parse()
	if flag.NArg() != 2 {
		usage()
		os.Exit(1)
	}

	ipAddr := flag.Args()[0]
	cmd := flag.Args()[1]

	plug := hsplug.NewPlug(ipAddr);

	if cmd == "on" {
		plug.TurnOn();
	} else {
		plug.TurnOff();
	}

	r, _ := plug.SystemInfo()
	fmt.Println("Light state:", r.RelayState)
}
